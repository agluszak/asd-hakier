#include <iostream>
#include <vector>
#include <queue>
#include <tuple>
#include <cassert>
#include <set>
#include <unordered_set>

using namespace std;
using num = size_t;


const num nextPowerOf2(num n) {
    num p = 1;
    if (n && !(n & (n - 1)))
        return n;

    while (p < n)
        p <<= 1;

    return p;
}

const size_t maxRange = 100000;
const size_t treeSize = nextPowerOf2(maxRange * 2);


struct Session {
    Session(size_t id, size_t start, size_t end) : id(id), start(start), end(end), empty(false) {
    }

    Session() : empty(true) {
    };
    size_t id;
    size_t start;
    size_t end;
    bool empty;

    inline bool operator==(const Session& rhs) {
        return this->empty == rhs.empty && this->id == rhs.id && this->start == rhs.start && this->end == rhs.end;
    }

    inline bool operator!=(const Session& rhs) {
        return !(*this == rhs);
    }

};

Session NO_SESSION = Session();

std::ostream& operator<<(std::ostream& stream, const Session& s) {
    if (!s.empty) {
        stream << "Session(" << s.id << ", [" << s.start << ", " << s.end << "])";
    } else {
        stream << "EmptySession";
    }
    return stream;
}

struct SessionSoonerEndComparator {
    bool operator()(const Session& a, const Session& b) {
        if (a.empty) {
            return false;
        }
        if (b.empty) {
            return true;
        }
        if (a.end == b.end) {
            return a.id < b.id;
        } else {
            return a.end < b.end;
        }
    }
};

struct SessionLaterEndComparator {
    bool operator()(const Session& a, const Session& b) {
        if (a.empty) {
            return false;
        }
        if (b.empty) {
            return true;
        }
        if (a.end == b.end) {
            return a.id < b.id;
        } else {
            return a.end > b.end;
        }
    }
};

template<typename T, typename MinOrdering, typename MaxOrdering>
class MinMaxSet {
    priority_queue<T, vector<T>, MinOrdering> min;
    priority_queue<T, vector<T>, MaxOrdering> max;
    unordered_set<size_t> deleted;
    size_t size;
    size_t deletedCount = 0;

    template<typename Ordering>
    T getTop(priority_queue<T, vector<T>, Ordering>& pq) {
        assert(!isEmpty());
        T top = pq.top();
        while (deleted.find(top.id) != deleted.end()) {
            pq.pop();
            top = pq.top();
        }
        return top;
    }

public:
    MinMaxSet(vector<T>& items) : size(items.size()),
                                  deleted(items.size()),
                                  min(items.begin(), items.end()),
                                  max(items.begin(), items.end()) {
    }

    bool isEmpty() {
        return size == deletedCount;
    }

    T getMin() {
        return getTop(min);
    }

    T getMax() {
        return getTop(max);
    }

    void remove(T item) {
        deleted.insert(item.id);
        deletedCount++;
    }


};

enum class ClueType {
    In, None, Some, Over
};

class Tree {
    using LeafSet = MinMaxSet<Session, SessionLaterEndComparator, SessionSoonerEndComparator>;
    vector<Session> minTree;
    vector<Session> maxTree;
    vector<LeafSet> leafSets;

    size_t left(size_t index) {
        return index * 2;
    }

    size_t right(size_t index) {
        return index * 2 + 1;
    }

    size_t getTreeIndex(size_t leafIndex) {
        return leafIndex + treeSize / 2;
    }

    template<typename Comparator>
    Session chooseBetter(Session first, Session second) {
        if (Comparator{}(first, second)) {
            return first;
        } else {
            return second;
        }
    }

    template<typename Comparator>
    void update(vector<Session>& tree, size_t treeIndex) {
        assert(treeIndex < treeSize);
        Session s = tree.at(treeIndex);
        while (treeIndex != 1) {
            treeIndex /= 2;
            Session leftChild = tree.at(left(treeIndex));
            Session rightChild = tree.at(right(treeIndex));
            tree.at(treeIndex) = chooseBetter<Comparator>(leftChild, rightChild);
        }
    }

    template<typename Comparator>
    Session query(vector<Session>& tree, size_t startIndex, size_t endIndex) {
        if (endIndex > maxRange) {
            return NO_SESSION;
        }
        size_t va = getTreeIndex(startIndex);
        size_t vb = getTreeIndex(endIndex);
        Session wyn = tree.at(va);
        if (va != vb) wyn = chooseBetter<Comparator>(wyn, tree.at(vb));
        while (va / 2 != vb / 2) {
            if (va % 2 == 0) wyn = chooseBetter<Comparator>(wyn, tree.at(va + 1));
            if (vb % 2 == 1) wyn = chooseBetter<Comparator>(wyn, tree.at(vb - 1));
            va /= 2;
            vb /= 2;
        }
        return wyn;
    }

    void remove(vector<Session>& tree, Session session) {
        assert(session != NO_SESSION);
        size_t start = session.start;
        size_t treeIndex = getTreeIndex(start);
        while (treeIndex != 1 && tree.at(treeIndex) == session) {
            tree.at(treeIndex) = NO_SESSION;
            treeIndex /= 2;
        }
    }

    void cleanup(Session session) {
        if (session == NO_SESSION) {
            return;
        }
        leafSets.at(session.start).remove(session);
        if (minTree.at(getTreeIndex(session.start)) == session) {
            remove(minTree, session);
            if (!leafSets.at(session.start).isEmpty()) {
                minTree.at(getTreeIndex(session.start)) = leafSets.at(session.start).getMin();
            }
            update<SessionSoonerEndComparator>(minTree, getTreeIndex(session.start));
        }
        if (maxTree.at(getTreeIndex(session.start)) == session) {
            remove(maxTree, session);
            if (!leafSets.at(session.start).isEmpty()) {
                maxTree.at(getTreeIndex(session.start)) = leafSets.at(session.start).getMax();
            }
            update<SessionLaterEndComparator>(maxTree, getTreeIndex(session.start));
        }
    }

public:
    Tree(vector<Session>& allSessions) :
            minTree(treeSize), maxTree(treeSize) {
        vector<vector<Session>> sessionsPerStart(maxRange + 1);
        for (Session& s: allSessions) {
            sessionsPerStart.at(s.start).push_back(s);
        }
        for (size_t i = 0; i <= maxRange; ++i) {
            leafSets.emplace_back(sessionsPerStart.at(i));
        }
        for (size_t i = 0; i <= maxRange; ++i) {
            if (!leafSets.at(i).isEmpty()) {
                minTree.at(getTreeIndex(i)) = leafSets.at(i).getMin();
                update<SessionSoonerEndComparator>(minTree, getTreeIndex(i));
            }
            if (!leafSets.at(i).isEmpty()) {
                maxTree.at(getTreeIndex(i)) = leafSets.at(i).getMax();
                update<SessionLaterEndComparator>(maxTree, getTreeIndex(i));
            }
        }
    }

    Session answer(ClueType type, size_t start, size_t end) {
        Session result;
        switch (type) {
            case ClueType::In:
                result = query<SessionSoonerEndComparator>(minTree, start, end);
                if (result.end > end) {
                    result = NO_SESSION;
                }
                break;
            case ClueType::None:
                result = query<SessionSoonerEndComparator>(minTree, 1, start);
                if (result == NO_SESSION || result.end >= start) {
                    result = query<SessionLaterEndComparator>(maxTree, end + 1, maxRange);
                }
                break;
            case ClueType::Some:
                result = query<SessionLaterEndComparator>(maxTree, 1, start);
                if (result == NO_SESSION || result.end < start) {
                    result = query<SessionSoonerEndComparator>(minTree, start, end);
                }
                break;
            case ClueType::Over:
                result = query<SessionLaterEndComparator>(maxTree, 1, start);
                if (result.end < end) {
                    result = NO_SESSION;
                }
                break;
        }
        cleanup(result);
        return result;
    }


};

void printResult(Session session) {
    if (session == NO_SESSION) {
        cout << -1 << " ";
    } else {
        cout << session.id << " ";
    }
}

int main() {
    vector<Session> sessions;
    size_t n;
    cin >> n;
    for (size_t i = 1; i <= n; ++i) {
        size_t start, end;
        cin >> start >> end;
        sessions.emplace_back(i, start, end);
    }
    Tree tree(sessions);
    size_t q;
    cin >> q;
    for (size_t i = 1; i <= q; ++i) {
        string type;
        size_t start, end;
        cin >> type >> start >> end;
        Session result;
        if (type == "in") {
            result = tree.answer(ClueType::In, start, end);
        } else if (type == "over") {
            result = tree.answer(ClueType::Over, start, end);
        } else if (type == "some") {
            result = tree.answer(ClueType::Some, start, end);
        } else if (type == "none") {
            result = tree.answer(ClueType::None, start, end);
        }
        printResult(result);
    }
    return 0;
}